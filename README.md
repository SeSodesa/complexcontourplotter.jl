# Complex Contour Plotter

This small package defines a complex contour plotter,
that allows one to produce color-coded contours of a function `f: ℂ → ℂ`.
The idea is that provided a curve on the domain of said function
and the values of the function, the Julia function provided by this library
will draw a figure with the curve on the domain of `f` in a left subfigure,
and the produced values of `f` in a right subfigure, with corresponding points
between the sets being the same color. This will hep visualize which point
on the domain of `f` mapped to which point on its co-domain.
