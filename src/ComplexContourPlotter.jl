module ComplexContourPlotter

const AUTHOR_MESSAGE = "ComplexContourPlotter.jl" *
    "\nCopyright © 2021 Santtu Söderholm"

greet() = print(AUTHOR_MESSAGE)

end # ComplexContourPlotter
